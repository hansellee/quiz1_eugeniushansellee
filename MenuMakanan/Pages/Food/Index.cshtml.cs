using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MenuMakanan.Models;
using MenuMakanan.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MenuMakanan.Pages.Food
{
    public class IndexModel : PageModel
    {
        private readonly ListFoodService _serviceMan;

        public IndexModel(ListFoodService listFoodService)
        {
            this._serviceMan = listFoodService;
        }

        public List<FoodModel> Foods { set; get; }

        //
        public async Task<IActionResult> OnPostDeleteAsync(int id)
        {
            var isSuccess = await this._serviceMan.DeleteListFoodAsync(id);

            if(isSuccess == false)
            {
                return Page();
            }
            return Redirect("/food"); 
        }
        
    }
}
