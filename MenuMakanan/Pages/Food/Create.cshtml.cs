using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MenuMakanan.Models;
using MenuMakanan.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MenuMakanan.Pages.Food
{
    public class CreateModel : PageModel
    {
        private readonly ListFoodService _serviceFood;

        public CreateModel(ListFoodService listFoodService)
        {
            this._serviceFood = listFoodService;
        }

        [BindProperty]
        public FoodModel NewFood { get; set; }
        public void OnGet()
        {
            NewFood = new FoodModel();
        }

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid == false)
            {
                return Page();
            }

            await _serviceFood.InsertNewFoodAsync(NewFood);
            return Redirect("./");
        }
    }
}
