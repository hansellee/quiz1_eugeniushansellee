using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MenuMakanan.Models;
using MenuMakanan.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MenuMakanan.Pages.Food
{
    public class UpdateModel : PageModel
    {
        private readonly ListFoodService _serviceFood;

        public UpdateModel(ListFoodService listFoodService)
        {
            this._serviceFood = listFoodService;
        }

        [BindProperty]
        public FoodModel CurrentFood { get; set; }

        public async Task OnGetAsync(int id)
        {
            CurrentFood = await _serviceFood.GetSpecificFoodAsync(id);
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if(ModelState.IsValid == false)
            {
                return Page();
            }

            var isExist = await _serviceFood.UpdateFoodAsync(CurrentFood);

            if (isExist == false)
            {
                return Page();
            }
            return Redirect("/food");
        }
    }
}

