﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MenuMakanan.Models;
using MenuMakanan.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MenuMakanan.API
{
    [Route("api/v1/food")]
    [ApiController]
    public class ListFoodController : ControllerBase
    {
        private readonly ListFoodService _serviceFood;

        public ListFoodController(ListFoodService listFoodService)
        {
            this._serviceFood = listFoodService;
        }
        // GET: api/<controller>
        [HttpGet("all-food", Name ="allFoodList")]
        public async Task<ActionResult<FoodModel>> GetAllFoodAsync(int makananId)
        {
            var foods = await _serviceFood.GetAllFoodAsync();
            return Ok(foods);
        }

        // GET api/<controller>/5
        [HttpGet("specific-food-list", Name = "GetSpecificFoodList")]
        public async Task<ActionResult<List<FoodModel>>> GetAllDataAsync(int? makananId)
        {
            if (makananId == 0)
            {
                return BadRequest(null);
            }

            var foods = await _serviceFood.GetSpecificFoodAsync(makananId.Value);

            if (foods == null)
            {
                return BadRequest(null);
            }
            return Ok(foods);
        }

        [HttpGet("specific-food", Name = "GetSpecificFood")]
        public async Task<ActionResult<FoodModel>> GetSpecificFood(string makananId)
        {
            if (string.IsNullOrEmpty(makananId) == true)
            {
                return BadRequest(null);
            }

            var id = Int32.Parse(makananId);
            var app = await _serviceFood.GetSpecificFoodAsync(id);

            if (app == null)
            {
                return BadRequest(null);
            }
            return Ok(app);
        }
        // POST api/<controller>
        [HttpPost("insert-food", Name = "insertListFood")]
        public async Task<ActionResult<FoodModel>> InsertNewFood([FromBody]FoodModel value)
        {
            var isSuccess = await _serviceFood.InsertNewFoodAsync(value);

            return Ok(new ResponseModel
            {
                ResponseMessage = "Success to insert new food!"
            });
        }

        // PUT api/<controller>/5
        [HttpPut("update-food", Name = "updateFood")]
        public async Task<ActionResult<ResponseModel>> UpdateFoodAsync([FromBody] FoodModel value)
        {
            var isSuccess = await _serviceFood.UpdateFoodAsync(value);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "ID Not Found!!!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success update data {value.NamaMakanan}"
            });
        }

        // DELETE api/<controller>/5
        [HttpDelete("delete-food", Name = "deleteFood")]
        public async Task<ActionResult<ResponseModel>> DeleteFoodAsync([FromBody] FoodModel value)
        {
            var isSuccess = await _serviceFood.DeleteListFoodAsync(value.MakananId);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Id not found!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success delete data {value.NamaMakanan}"
            });
        }

        [HttpGet("filter-data")]
        public async Task<ActionResult<List<FoodModel>>> GetFilterDataAsync([FromQuery] int pageIndex, int itemPerPage, string filterByName)
        {
            var data = await _serviceFood.GetPageAsync(pageIndex, itemPerPage, filterByName);

            return Ok(data);
        }

        [HttpGet("total-data")]
        public ActionResult<int> GetTotalData()
        {
            var totalData = _serviceFood.GetTotalData();

            return Ok(totalData);
        }
    }
}
