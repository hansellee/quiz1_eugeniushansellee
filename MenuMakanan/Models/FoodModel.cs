﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MenuMakanan.Models
{
    public class FoodModel
    {
        [Required]
        public int MakananId { get; set; }
        [Required]
        [MaxLength(7), MinLength(3)]
        [Display(Name = "Nama Makanan")]
        public string NamaMakanan { get; set; }
        [Required]
        [Display(Name = "Jumlah Makanan")]
        public int JumlahMakanan { get; set; }
    }
}
