﻿using MenuMakanan.Entities;
using MenuMakanan.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MenuMakanan.Services
{
    public class FoodService
    {
        private readonly ListFoodDbContext _db;

        public FoodService(ListFoodDbContext dbContext)
        {
            this._db = dbContext;
        }

        public async Task<bool> InsertNewFoodAsync(FoodModel foodModel)
        {
            this._db.Add(new ListFood
            {
                NamaMakanan = foodModel.NamaMakanan,
                JumlahMakanan = foodModel.JumlahMakanan
            });

            await this._db.SaveChangesAsync();
            return true;
        }

        public async Task<List<FoodModel>> GetAllFoodAsync()
        {
            var menuMakanan = await this._db
                .ListFoods
                .Select(Q => new FoodModel
                {
                    MakananId = Q.MakananId,
                    NamaMakanan = Q.NamaMakanan,
                    JumlahMakanan = Q.JumlahMakanan
                })
                .AsNoTracking()
                .ToListAsync();

            return menuMakanan;
        }

        public async Task<List<FoodModel>> GetPageAsync(int pageIndex, int itemPerPage, string filterByName)
        {
            var query = this._db
                .ListFoods
                .AsQueryable();

            if (string.IsNullOrEmpty(filterByName) == false)
            {
                query = query
                    .Where(Q => Q.NamaMakanan.StartsWith(filterByName));
            }

            var foods = await query
                .Select(Q => new FoodModel
                {
                    MakananId = Q.MakananId,
                    NamaMakanan = Q.NamaMakanan,
                    JumlahMakanan = Q.JumlahMakanan
                })
                .Skip((pageIndex - 1) * itemPerPage)
                .Take(itemPerPage)
                .AsNoTracking()
                .ToListAsync();

            return foods;
        }

        public int GetTotalData()
        {
            var totalFoods = this._db
                .ListFoods
                .Count();

            return totalFoods;
        }

        public async Task<bool> DeleteListFoodAsync(int id)
        {
            var menuMakanan = await this._db
                .ListFoods
                .Where(Q => Q.MakananId == id)
                .FirstOrDefaultAsync();

            if (menuMakanan == null)
            {
                return false;
            }
            this._db.Remove(menuMakanan);
            await this._db.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdateFoodAsync(FoodModel makanan)
        {
            var menuMakanan = await this._db
                .ListFoods
                .Where(Q => Q.MakananId == makanan.MakananId)
                .FirstOrDefaultAsync();
            if (menuMakanan == null)
            {
                return false;
            }

            menuMakanan.NamaMakanan = makanan.NamaMakanan;
            menuMakanan.JumlahMakanan = makanan.JumlahMakanan;
            await this._db.SaveChangesAsync();
            return true;
        }

        public async Task<FoodModel> GetSpecificFoodAsync(int makananId)
        {
            var foods = await this._db
                .ListFoods
                .Where(Q => Q.MakananId == makananId)
                .Select(Q => new FoodModel
                {
                   MakananId = Q.MakananId,
                   NamaMakanan = Q.NamaMakanan,
                   JumlahMakanan = Q.JumlahMakanan
                })
                .FirstOrDefaultAsync();

            return foods;
        }

        public async Task<List<FoodModel>> GetAsync(int? makananId)
        {
            var foods = await this._db
                .ListFoods
                .Where(Q => Q.MakananId == makananId)
                .Select(Q => new FoodModel
                {
                    MakananId = Q.MakananId,
                    NamaMakanan = Q.NamaMakanan,
                    JumlahMakanan = Q.JumlahMakanan
                })
                .AsNoTracking()
                .ToListAsync();

            return foods;
        }


    }
}
