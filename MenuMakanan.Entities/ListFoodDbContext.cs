﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace MenuMakanan.Entities
{
    public class ListFoodDbContext :DbContext
    {
        public ListFoodDbContext(DbContextOptions<ListFoodDbContext> options) : base(options)
        {

        }
        public DbSet<ListFood> ListFoods { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ListFood>().ToTable("foods");
        }
    }
}
