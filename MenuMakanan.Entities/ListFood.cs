﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MenuMakanan.Entities
{
    public class ListFood
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MakananId { get; set; }
        [Required]
        public string NamaMakanan { get; set; }
        [Required]
        public int JumlahMakanan { get; set; }
    }
}
