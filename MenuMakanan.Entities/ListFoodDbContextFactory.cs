﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace MenuMakanan.Entities
{
    public class ListFoodDbContextFactory : IDesignTimeDbContextFactory<ListFoodDbContext>
    {
        public ListFoodDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ListFoodDbContext>();
            builder.UseSqlite("Data Source=reference.db");
            return new ListFoodDbContext(builder.Options);
        }
    }
}
